from modulo import modulo as Mod
def coef(P,n): #Function that extracts coefficient from a modular polynomial
    p = len(P[0])
    if n>=len(P):
        return Mod(0,p)
    else:
        return P[n]
def trim(P):
    p=len(P[0])
    deg = len(P)-1
    n = deg
    while(n > 0 and int(coef(P,n)) == 0):
        n=n-1
    return(P[0:n+1])


def add(P1,P2): #Function to add to modular polynomials
    p=len(P1[0]) #Extracts the modulus.
    deg = max(len(P1) ,len (P2)) - 1
    result = (deg+1)*[Mod(0,p)]
    for n in range(deg+1):
        result[n] = coef(P1,n) + coef(P2,n)
    return(result)

def scale(a,P):
    deg = len(P) -1
    p = len(P[0])
    result=(deg+1)*[Mod(0,p)]
    for n in range(deg+1):
       result[n]=P[n]*a
    return(result)

def rem(P,Q):
    p=len(P[0])
    deg1=len(P)-1
    deg2=len(Q)-1
    if deg2>deg1 or int(P[deg1]) == 0:
        return P
    else:
        shift=(deg1-deg2)*[Mod(0,p)]+Q
        c = -(P[deg1] // Q[deg2])
        result = add(P,scale(c,shift))
        result = trim(result)
        return(rem(result,Q))
def divisible(P,Q):
    R = rem(P,Q)
    return(len(R)==1 and int(R[0])==0)
