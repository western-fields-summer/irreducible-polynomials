from modulo import modulo as Mod
from itertools import product
from longDivision import divisible
def main():
 for p in [2,3,5]:
   L2 , L3 , L4 = [] , [] , []
   for a,b in product(range(p),range(p)): #Populate L2
       flag = True
       x = Mod(0,p)
       a = Mod(a,p)
       b = Mod(b,p)
       k=0
       while flag and k<p:
           if int(x**2+a*x+b)  == 0:
               flag=False
           x = x + Mod(1,p)
           k = k+1
       if flag:
           L2.append([b,a,Mod(1,p)])

   for a,b,c in product(range(p),range(p),range(p)): #Populate L3
       flag = True
       x,a,b,c= Mod(0,p),Mod(a,p),Mod(b,p),Mod(c,p)
       k=0
       while flag and k<p:
           if(int(x**3+a*x**2+b*x+c)==0):
               flag=False
           x=x+Mod(1,p)
           k=k+1
       if flag:
           L3.append([c,b,a,Mod(1,p)])
   for a,b,c,d in product(range(p),range(p),range(p),range(p)):
       flag = True
       x,a,b,c,d=Mod(0,p),Mod(a,p),Mod(b,p),Mod(c,p),Mod(d,p)
       k=0
       while flag and k<p:
           if(int(x**4+a*x**3+b*x**2+c*x+d)==0):
               flag=False
           x=x+Mod(1,p)
           k=k+1
       k=0
       while flag and k<len(L2):
           if divisible([d,c,b,a,Mod(1,p)],L2[k]):
               flag = False
           k=k+1
       if flag:
           L4.append([Mod(1,p),a,b,c,d])
   print("Modulo %d, the irreducible polynomials of degree 2 are"%p)
   for P in L2:
     print("X^2+%dX+%d"%(int(P[1]),int(P[0])))
   print("Modulo %d, the irreducible polynomials of degree 3 are"%p)
   for P in L3:
     print("X^3+%dX^2+%dX+%d"%(int(P[2]),int(P[1]),int(P[0])))
   print("Modulo %d, the irreducible polynomials of degree 4 are"%p)
   for P in L4:
     print("X^4+%dX^3+%dX^2+%dX+%d"%(int(P[3]),int(P[2]),int(P[1]),int(P[0])))
if __name__== '__main__':
    main()
